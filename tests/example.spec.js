
test('example 2', async ({ page }) => {
  // aut
   await page.goto('https://app-dev.dev.quar.cz');
  const emailSignIn = page.locator("[name = 'email']");
  const rememberMeCheckBox = page.locator(".remember-checkbox");
  const nextBtn = page.locator("//button[text()='Next']");
  const passwordSignIn = page.locator("[name = 'password']");
   await emailSignIn.type("zhukovak@proverstka.com");
   await rememberMeCheckBox.check();
   await nextBtn.click();
   await passwordSignIn.type("Test5545");
   await nextBtn.click();
  const education = page.locator("//a[text()='Education']")
  await education.click();
  //
  const searchField = page.locator("[name = 'search']");
   await searchField.type("Must have");
  const searchBtn = page.locator("//button[text()='Search']");
   await searchBtn.click();
  const findTitile = page.locator("[title = 'Must have']");
   await expect(findTitile).toContainText('Must have');
});
